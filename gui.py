import os
import manager
import serial.tools.list_ports
import time
import tkinter
import tkinter.ttk
import tkinter.filedialog
import threading

class ApplicationGui:
    def __init__(self, parent):
        self.parent = parent
        tkinter.Grid.columnconfigure(self.parent, 0, weight=1)
        tkinter.Grid.rowconfigure(self.parent, 0, weight=1)
        tkinter.Grid.rowconfigure(self.parent, 1, weight=1)
        tkinter.Grid.rowconfigure(self.parent, 2, weight=1)
        self.show_connection_page()

    def show_connection_page(self):
        self.label = tkinter.Label(self.parent, text="Выберите порт")
        self.label.grid(row=0, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

        self.combobox = tkinter.ttk.Combobox(self.parent)
        self.combobox['values'] = [port[0] for port in serial.tools.list_ports.comports()]
        self.combobox.current(0)
        self.combobox.grid(row=1, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

        self.button = tkinter.Button(self.parent, text="Подключиться", command=self.connect_button_click)
        self.button.grid(row=2, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

    def connect_button_click(self):
        self.manager = manager.Manager(self.combobox.current(), self)

        self.label.destroy()
        self.combobox.destroy()
        self.button.destroy()

        self.show_main_page()

    def show_main_page(self):
        menubar = tkinter.Menu(self.parent)
        self.parent.config(menu=menubar)

        self.filemenu = tkinter.Menu(menubar, tearoff=False)
        self.filemenu.add_command(label="Отправить", command=self.show_open_dialog)
        self.filemenu.add_command(label="Принять", command=self.show_save_dialog)
        self.filemenu.add_command(label="Выход", command=self.on_exit)
        self.filemenu.entryconfig(1, state=tkinter.DISABLED)

        menubar.add_cascade(label="Файл", menu=self.filemenu)

        self.label = tkinter.Label(self.parent, text="Выберите файл для отправки")
        self.label.grid(row=0, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

        self.progressbar = tkinter.ttk.Progressbar(self.parent, maximum=100)
        self.progressbar.grid(row=1, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

        self.indicator = tkinter.Label(self.parent, foreground="white")
        self.indicator.grid(row=2, padx=10, pady=10, sticky=tkinter.N+tkinter.S+tkinter.W+tkinter.E)

        self._thread_check = threading.Thread(target=self.connection_checker)
        self._thread_check.start()

    def connection_checker(self):
        while True:
            if self.manager.serial_port.getCD():
                self.indicator['text'] = "Соединение установлено"
                self.indicator['background'] = "green"
            else:
                self.indicator['text'] = "Нет соединения"
                self.indicator['background'] = "red"
            time.sleep(1)

    def on_exit(self):
        if hasattr(self, "_thread_check"):
            self._thread_check._stop()
            self.manager._thread_write._stop()
            self.manager._thread_read._stop()
            self._thread_check.join()
            self.manager._thread_write.join()
            self.manager._thread_read.join()
        self.parent.quit()

    def show_open_dialog(self):
        self.progressbar['value'] = 0
        self.manager.send_file(tkinter.filedialog.askopenfilename())
        self.label['text'] = "Начинается передача файла"

    def show_save_dialog(self):
        filename, extension = os.path.splitext(self.manager._filename)
        filename = os.path.basename(filename)
        self.progressbar['value'] = 0
        self.manager.recv_file(tkinter.filedialog.asksaveasfilename(
            defaultextension=extension, initialfile=filename,
            filetypes=(("{0} файл".format(extension[1:]), "*{0}".format(extension)),)))
        self.label['text'] = "Начинается приём файла"
        self.filemenu.entryconfig(1, state=tkinter.DISABLED)

    def update_progressbar(self, percent):
        self.progressbar['value'] = percent
        self.label['text'] = "Завершено {0} %".format(percent)

if __name__ == "__main__":
    root = tkinter.Tk()
    root.wm_title("Com File")
    root.minsize(350, 150)
    app = ApplicationGui(root)
    root.protocol("WM_DELETE_WINDOW", app.on_exit)
    root.mainloop()