import file
import frames
import pickle
import serial
import threading
import time
import tkinter
import tkinter.filedialog

DATA_SIZE = 256


class Manager:
    def __init__(self, number, ui):
        self.ui = ui
        self.serial_port = serial.Serial(number, 115200)
        self._thread_read = threading.Thread(target=self.wait_for_input)
        self._thread_read.start()
        self.send_flag = 0
        self._thread_write = threading.Thread(target=self.wait_for_output)
        self._thread_write.start()

    def wait_for_input(self):
        while True:
            while self.serial_port.inWaiting() == 0:
                time.sleep(0.1)
            bytes_in_buffer = self.serial_port.inWaiting()
            time.sleep(0.1)
            # Начат приём данных, ждём момента когда передатчик передаст все данные
            while bytes_in_buffer != self.serial_port.inWaiting():
                bytes_in_buffer = self.serial_port.inWaiting()
                time.sleep(0.1)
            while not self.serial_port.getCD():
                self.serial_port.flushInput()
                continue
            self.process_input()

    def wait_for_output(self):
        while True:
            while self.send_flag == 0:
                time.sleep(0.1)
            while not self.serial_port.getCD():
                time.sleep(1)
            self.process_output()

    def process_input(self):
        # Если нечего читать, то значит, что буфер был очищен из-за потери соединения.
        # Поэтому нужно запросить повторную отправку кадра и вернуться из функции.
        if self.serial_port.inWaiting() == 0:
            self.send_flag = 4
            return
        data = self.serial_port.read(self.serial_port.inWaiting())
        frame = pickle.loads(data)

        # Определение типа полученного кадра и установка флага отправки кадра в ответ
        if isinstance(frame, frames.AcknowledgeFrame):
            self.send_flag = 2
        elif isinstance(frame, frames.InformationFrame):
            if not frame.decode():
                self.send_flag = 4
                return
            self.file.write(frame.data)
            self.send_flag = 1
            # Если файл получен полностью, то закрываем его
            if self.file.is_done():
                self.file.close()
        elif isinstance(frame, frames.InitializationFrame):
            self.ui.filemenu.entryconfig(0, state=tkinter.DISABLED)
            self.ui.filemenu.entryconfig(1, state=tkinter.NORMAL)
            self.ui.label['text'] = "Подтвердите приём файла"
            self._filesize = frame.filesize
            self._filename = frame.filename
            return
        elif isinstance(frame, frames.RetransmitFrame):
            # Будет отправлен последний отправленный кадр
            self.send_flag = 5
        percent = self.file.get_percent()
        self.ui.update_progressbar(percent)
        if percent == 100:
            self.ui.filemenu.entryconfig(0, state=tkinter.NORMAL)
            self.ui.filemenu.entryconfig(1, state=tkinter.DISABLED)

    # Отправляем кадр в соответсвии с установленным флагом
    def process_output(self):
        if self.send_flag == 1:
            self.last_sent_frame = pickle.dumps(frames.AcknowledgeFrame())
        elif self.send_flag == 2:
            # Завершаем передачу, если файл передан полностью
            if not self.file.is_done():
                frame = frames.InformationFrame(self.file.read(DATA_SIZE))
                frame.encode()
                self.last_sent_frame = pickle.dumps(frame)
            else:
                self.file.close()
                self.send_flag = 0
                return
        elif self.send_flag == 3:
            self.last_sent_frame = pickle.dumps(frames.InitializationFrame(self.file.name, self.file.size))
            self.ui.filemenu.entryconfig(0, state=tkinter.DISABLED)
            self.ui.filemenu.entryconfig(1, state=tkinter.DISABLED)
        elif self.send_flag == 4:
            self.last_sent_frame = pickle.dumps(frames.RetransmitFrame())
        # Если нет соединения, то возвращается из функции и запрашиваем отправку последнего кадра
        if not self.serial_port.getCD():
            self.send_flag = 5
            return
        self.serial_port.write(self.last_sent_frame)
        self.send_flag = 0
        percent = self.file.get_percent()
        self.ui.update_progressbar(percent)
        if percent == 100:
            self.ui.filemenu.entryconfig(0, state=tkinter.NORMAL)
            self.ui.filemenu.entryconfig(1, state=tkinter.DISABLED)

    def send_file(self, filename):
        self.file = file.File(filename, "rb")
        self.send_flag = 3

    def recv_file(self, filename):
        self.file = file.File(filename, "wb", self._filesize)
        self.send_flag = 1