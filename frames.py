from hamming_code import HammingCode


class InitializationFrame:
    def __init__(self, filename, filesize):
        self.filename = filename
        self.filesize = filesize


class InformationFrame:
    def __init__(self, data):
        self.data = data

    def encode(self):
        encoded_data = ''.encode()
        for i in range(len(self.data)):
            left_byte, right_byte = HammingCode.encode(self.data[i])
            encoded_data += bytes([left_byte])
            encoded_data += bytes([right_byte])
        self.data = encoded_data

    def decode(self):
        decoded_data = ''.encode()
        for i in range(0, len(self.data), 2):
            res_byte = HammingCode.decode(self.data[i], self.data[i + 1])
            if res_byte == -1:
                return False
            decoded_data += bytes([res_byte])
        self.data = decoded_data
        return True


class AcknowledgeFrame:
    pass


class RetransmitFrame:
    pass