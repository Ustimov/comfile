import os


class File:
    def __init__(self, name, mode, size=0):
        self.name = name
        if mode == "rb":
            self.size = os.stat(name).st_size
            self._current_size = 0
        elif mode == "wb":
            self.size = size
            self._current_size = 0
        self._file = open(name, mode)

    def read(self, size):
        # Если запрошено для чтения больше байт, чем есть в файле,
        # то установить текущий размер файла равным его полному размеру
        if self._current_size + size < self.size:
            self._current_size += size
        else:
            self._current_size = self.size
        return self._file.read(size)

    def write(self, data):
        self._current_size += len(data)
        self._file.write(data)

    def get_percent(self):
        return int(100 * self._current_size / self.size)

    # Возвращает True, если чтение или запись завершены
    def is_done(self):
        return self._current_size == self.size

    def close(self):
        self._file.close()