class HammingCode:
    @staticmethod
    def encode(byte):
        byte_arr = [False for i in range(8)]
        left_arr = [False for i in range(8)]
        right_arr = [False for i in range(8)]
        HammingCode.byte_to_array(byte, byte_arr)

        half_arr = right_arr
        shift = 0
        for i in range(2):
            half_arr[2] = byte_arr[0 + shift]
            half_arr[4] = byte_arr[1 + shift]
            half_arr[5] = byte_arr[2 + shift]
            half_arr[6] = byte_arr[3 + shift]
            half_arr[7] = False

            half_arr[0] = half_arr[2] ^ half_arr[4] ^ half_arr[6]
            half_arr[1] = half_arr[2] ^ half_arr[5] ^ half_arr[6]
            half_arr[3] = half_arr[4] ^ half_arr[5] ^ half_arr[6]

            half_arr = left_arr
            shift += 4
        left = HammingCode.array_to_byte(left_arr)
        right = HammingCode.array_to_byte(right_arr)
        return left, right

    @staticmethod
    def decode(left, right):
        leftOK = HammingCode.check_for_error(left)
        rightOK = HammingCode.check_for_error(right)
        if not leftOK or not rightOK:
            return -1
        byte_arr = [0 for i in range(8)]
        left_arr = [0 for i in range(8)]
        right_arr = [0 for i in range(8)]
        HammingCode.byte_to_array(left, left_arr)
        HammingCode.byte_to_array(right, right_arr)
        half_arr = right_arr
        shift = 0
        for i in range(2):
            byte_arr[0 + shift] = half_arr[2]
            byte_arr[1 + shift] = half_arr[4]
            byte_arr[2 + shift] = half_arr[5]
            byte_arr[3 + shift] = half_arr[6]
            half_arr = left_arr
            shift += 4
        return HammingCode.array_to_byte(byte_arr)

    @staticmethod
    def check_for_error(byte):
        byte_arr = [0 for i in range(8)]
        HammingCode.byte_to_array(byte, byte_arr)
        syndrome = 1 * (byte_arr[0] ^ byte_arr[2] ^ byte_arr[4] ^ byte_arr[6]) + 2 * \
            (byte_arr[1] ^ byte_arr[2] ^ byte_arr[5] ^ byte_arr[6]) + 4 * \
            (byte_arr[3] ^ byte_arr[4] ^ byte_arr[5] ^ byte_arr[6])
        syndrome -= 1
        if syndrome > -1:
            return False
        else:
            return True

    @staticmethod
    def byte_to_array(byte, array):
        for i in range(8):
            array[i] = (byte >> i) & 1

    @staticmethod
    def array_to_byte(array):
        byte = 0
        p = 1
        for i in range(8):
            byte += array[i] * p
            p *= 2
        return byte
